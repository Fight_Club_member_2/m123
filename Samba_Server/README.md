## How to set up Fileshare / Samba Server on Linux 22.04


[TOC]

## 1. Installation des Samba Server 
![Alt text](image.png)

- Zuerst intallieren wir den Samba server das machen wir mit dem Befehl **sudo apt install samba**. Danach müssen sie noch Ihr Passwort eingebeb und danach läuft der Installationsprozess automatisch ab.

## 2. Neuer Benutzer erstellen
![Alt text](image-1.png)
- Nun erstellen wir einen neuen benutzer dies machen wir mit dem Befehl **sudo adduser sambauser**.

![Alt text](image-2.png)

- Und nun muss man dem neuen Benutzer noch ein neues Passwort geben dies macht man mit dem Befehl **sudo smbpasswd -a sambauser**.

## 3. Shared folder Konfigurieren
![Alt text](image-3.png)

- Nun gehen wir mit dem Befehl **nano /etc/samba/smb.conf** in die Konfigurationsdatei. Dort Scrollen wir bis ganz nach unten und schreiben von hand **[shared_folder]** und dort schreibt man untendran alle punkte auf dem oberen Bild zu sehen sind. Nachdem speichern sie die Änderungen und verlassen die Konfigurationsdatei.

![Alt text](image-4.png)
- Nun müssen wir nur noch den Server neu starten mit dem Befehl **service smbd restart**.

## 4. Testing 

**Client**

![Alt text](image-5.png)

- Nun geben sie bei dem Client die IP-Adresse von dem Server ein die wäre in meinem Fall **192.168.8.146**. Danach sollte der Shared folder Ordner erscheinen und nachach muss man sich mit mit dem Sambauser Account anmelden.

![Alt text](image-7.png)

- Danach macht man ein Text dokument und schreibt dort etwas rein und speichert es. Danach Überprüfen wir im Server ob die änderungen übernommen wurde. 

**Server**
![Alt text](image-8.png)

- Nun sehen wir das die Änderungen auch im server angezeigt werden und somit funktioniert der Samba Server einwandfrei.


Danke fürs Lesen!

**END!**