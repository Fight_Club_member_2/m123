# DHCP Configuration NEU
In dieser Dokumentation wird erklärt, wie man auf Filius einen **DHCP-Server** aufsetzt und konfiguriert.

## DHCP Setup

![Architektur](./Images/1.png)

**1.** HIer können sie den ersten schritt sehen den wir durchführen werden.
Das wäre die Verknüpfung alles Komponenten in diesem Netzwerk.
  
![Architektur](./Images/2.png)

**2.** Nach dem ersten Schritt gehen sie auf auf den DHCP-Server und gehen unten Rechts auf den Reiter DHCP-Server einrichten.

![Architektur](./Images/4.png)

**3.** Bei dem 3ten Schritt müssen sie die auf dem oberen Bild stehende Mac-Adresse und ebenso die IP-Adresse in diese 2 Linien einfügen. Und danach auf Hinzufügen drücken nach diesem schritt sollte bei ihnen ebenso ein Balken mit beidem drin untendran aufploppen. Ist dies der Fall haben sie alles Richtig gemacht.

![Architektur](./Images/3.png)

**4.** Nach dem 3ten Schritt ist nicht mehr viel zu tun danach müssen sie nur noch den DHCP aktivieren klicken und schon haben sie einen astreinen DHCP-Server Konfiguriert.


**5.** Danach ist nur noch der letzte Schritt zu tun. In diesem Schritt überprüfen wir anhand von einem Ping ob der DHCP-Server auch wircklich funktioniert. Das kann man sehen ob es einen Paket verlust gibt oder nicht. Wenn ja dann soltten sie die Anleitung nochmals in ruhe durchgehen.

Ansonsten sind sie am Ende Angelangt gut gemacht! Jetzt wissen welche Schritte sie machen müssen das sie einen DHCP-Server Konfigurieren können.

## Auftrag 2 Labor übung Cisco Paket Tracer

**1. Router-Konfiguration auslesen** 

- Zuerst habe ich mit hilfe von verschiedenen Befehlen Cli heraus gefunden wie mein Router Konfiguriert ist.  Und danach wusste ich bescheid wie alles verfügbar und Konfiguriert ist.
- Die befehle sind:


- show ip dhcp pool
- show ip dhcp binding
- show running-config

![Alt text](image.png)

**2. DORA - DHCP Lease beobachten**

- Hier habe ich den Prozess was zwischen dem Client und dem DHCP passiert. Denn dieser Prozess ist much entscheidend. Der Dora Prozess ist der Gröste bestandteil von der Komunikation.


![Alt text](image-1.png)

- Die Packete starten bei PC2 und werden jetz per Brodcast an den DHCP server weitergeleitet. Danach schickt der DHCP Server eine Offerte zurück die der PC2 (Client 2) wieder per Brodcast bekommt und animmt.


**3. Netzwerk umkonfigurieren**

- Der OP-Code für den DHCP-Offer ist 2

- Der OP-Code für den DHCP-Request ist 3


- Der OP-Code für den DHCP-Acknowledge ist 5

- Der DHCP-Discover wird normalerweise an die Broadcast-IP-Adresse 255.255.255.255 gesendet.

- Der DHCP-Discover wird normalerweise an die Broadcast-MAC-Adresse ff:ff:ff:ff:ff:ff gesendet

- Dies könnte darauf hindeuten, dass der Switch die Broadcasts weiterleitet, um sicherzustellen, dass alle Geräte im Netzwerk die Möglichkeit haben, auf den DHCP-Discover zu antworten. Dies ist üblich in Netzwerken ohne VLANs oder subnetzübergreifender Konfiguration.

-

-

-
