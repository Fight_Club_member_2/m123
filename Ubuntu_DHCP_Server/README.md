# Einrichten eines DHCP-Servers auf einem Ubuntu-Server

In diesem Dokument wird beschrieben, wie Sie einen DHCP-Server auf einem Ubuntu-Server einrichten und Konfigurieren können.

## Schritt 1: Installation von DHCP-Server
![Alt text](image.png)

- Zuerst melden sie sich an mit ihrem Login das sie Bereit sind und den Zugriff haben Veränderungen an dem Server vorzunehmen.


## Schritt 2: Finden sie die IP Adresse und den ens Ihres Servers heraus
![Alt text](image-1.png)

- Hier für geben sie den Befehl **ip a** und schon erhalten sie die IP Adresse und den ens. Diese befinden sich nach dem 2: Punkt das bei mir **ens33 und 192.168.8.137 ist**.

## Schritt 3: Installation des DHCP Dienstes
- Der nächste Schritt beinhaltet die Installation des DHCP servers den man mit dem Kommando: **sudo apt install -y isc-dhcp-server** Installiert. Nach dem sie die Installation mit Ihrem Kennwort bestätigt haben wird der DHCP dienst installiert.



![Alt text](image-3.png)
- 

## Schritt 4: Öffnen der DHCP Konfigurations datei
- Nun muss die erste Veränderung vorgenommen werden in der DHCP datei. In diese Datei gelangt man mit Folgendem Befehl: **sudo nano /etc/default/isc-dhcp-server**. Es muss ebenfalls bei diesem Schritt das Kennwort eingegben werden.

![Alt text](image-4.png)

-  Nach der eingabe Ihres Kennworts kommt man auf diese seite wo man bei **INTERFACESv4** sein ens Code einfügen den man bei IP A nachgeschaut hat.
Danach verlässt man das fenster mit der Tastenkombination **Ctrl + X** und speichert im anschliesenden fenster die Veränderung.

## Schritt 5: Konfuguration des DHCP Adressenpools

- Öffnen sie die Datei mit dem Befehl: **sudo nano /etc/dhcp/dhcpd.conf**.

![Alt text](image-5.png)

![Alt text](image-7.png)

- Wenn man in der Konfigurationsdatei angelangt ist scrollt man runter bis zu dem Reiter # A Slightly.... und und dort konfiguriert man alles wie es auf dem Bild zu sehen ist. Danach speichern man die Änderungen und verlässt das Dokument.

## Schritt 6: Neustart des DHCP Dienstes / Status Überprüfen

- Denn Neustart führt man mit dem Befehl: **sudo systemctl restart isc-dhcp-server** aus.

- Danach überprüft man den Status des Servers. Dies macht man mit dem Befehl:**sudo systemctl status isc-dhcp-server**

![Alt text](image-10.png)

- Hier muss active stehen.

## Schritt 7: Überprüfung 
![Alt text](image-12.png)
- Nun können sie im CMD eines Clientes herausfinden ob es geklappt hat.

## Feheler Meldungen
![Alt text](image-13.png)

- Nach einer weile und ein paar Änderungen bekam ich bei dem Befehl sudo systemctl status... immmer diese fehelermeldung erhalten und egal was ich versucht habe konnte ich die Ursache des Problems nicht finden.


## Fazit

- Diese Arbeit hat mir sehr viel spass gemacht und ich konnte sehr viel neue erfahrungen machen im bereich Server und arbeiten in der Kommandozeile. Dazu bin ich auch von den Befehlen her viel sicherer geworden und auch in der Fehlerbehebung.Ausserdem konnte ich den DHCP auch zum laufen bringen und er funktionierte auch. Aber nach ein paar Änderungen bekamm ich bei der Status meldung immer ein failed und diese meldung konnte ich auch selber nicht beheben. Jedoch konnte ich viel nützliches für die Zukunft mitnehemn.