## How to set up an DHCP, DNS, an Web Server on Filius


[TOC]

## 1. DHCP Server

![Alt text](image.png)

- Bei dem DHCP Server müssen wir in den Einstellungen die Range eingeben Mit welchen IP Adressen er die Clients bestücken sollte. In diesem Fall ist das **10.0.0.3-10.0.0.10** Und im Unteren Fester kann man ebenfalls gerade noch den Gateway und den DNS Server Eintragen.

![Alt text](image-2.png)

- Wie man hier mit Befehl **ipconfig** sehen kann funktioniert der DHCP Server einwandfrei.
## 2. DNS Server

![Alt text](image-3.png)

- Bei dem DNS Server muss man auf dem Server das DNS Programm installieren und danach Jeder Client mit seinem Namen und seiner IP Adresse hier anmelden das bringt uns nacher denn vorteil das man die Clients auch mit dem Namen Pingen kann.

![Alt text](image-4.png)

- Wie man hier oben sehen kann Funktioniert mein DNS Server da ich einen Ping an **PC21** machen konnte ohne das ich die IP Adresse verwenden musste.

## 3. Web Server

![Alt text](image-5.png)

- Um den Web server zu verwenden müssen wir eigentlich nur eine sache tun und zwar muss man noch den **Gateway** und den **Domain Name Server** angeben. Dazu nehme ich die Adresse des **DNS Servers**.

![Alt text](image-3.png)

- Dazu habe ich noch die Die Google Website bei dem DNS Server hinzugefügt und dazu die IP Adresse des Web Servers.

![Alt text](image-6.png)

- Wie man sehen kann konnte die Website ohne probleme Gefunden Werden.

Danke fürs Lesen!

**END!**