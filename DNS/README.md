## Set up DNS Server on Windows 10 


[TOC]

## 1. Installation 

![Alt text](image-4.png)

- Nach dem der Server Aufgestartet ist sehen sie das Windows Server Menü. Als den ersten schritt wählen sie hier die Nummer 2 aus **Add roles and features**.

![Alt text](Untitled.png)

- Danach erscheint das Fenster von Add roles and features. Dort drücken sie bei jedem Fenster auf Next bis sie zu dem Reiter Server Roles kommen. Dort wählen sie den Reiter **DNS Server** an und Drücken ebenfalls Next. Danach drücken sie wieder Next bis zum reiter **Results** und dort Klicken sie Install. Danach Wird der DNS Server installiert und sobald die Installation abgeschlossen ist wird ihr Gerät neu gestartet.


**Einbindung der 2 Netzwerkarte**

![Alt text](Untitled3.png)

- Nun folg die einbindung der Zweiten Netzwerkarte hierfür gehen sie bei Beiden Maschinen in die **Settings** unter **VM** und danach auf den untersten reiter Settings. Und schon kommen sie auf Dieses Fenster (Unteres Bild).

![Alt text](image-3.png)

- Wenn sie Hier angelangt sind gehen sie auf **Add** und wählen Network Adapter. Danach schalten sie diesen von Nat auf Costom um und wählen ihr Privates erstelltes netzwerk jetzt in diesem fall von dem Modul 117 **M117_Lab**.

## 2. Konfiguration

![Alt text](Untitled1.png)

- Nach dem Neustart wird in ihrem Server Manager der Balken **DNS** angezeigt dies bestätigt erneut das die Installation Erfolgreich war.

![Alt text](Untitled2.png)

- Danach gehen sie Oben rechts auf Tools und Wählen **DNS** aus und warten bis sich der **DNS Manager** öffnet. 

- Als nächstes gehen sie mit dem Rechtsklick auf die **Forward Zones & Reverse lockup zone** und erstellen eine Neue Zone. Bei bei beiden Zonen Drücken sie immer auf **Next** bis sie zum Feld Namen Kommen dort schreiben sie einen Belibigen namen rein. In meinem Fall wäre das Server und Client jedoch können sie dies selbst entscheiden.

![Alt text](image-1.png)

- wie sie sehen können habe ich hier einen neuen Host erstellt (AAA) mit **Rechtsklick in der neuen Zone** Dabei geben sie im obersten fenster einen Namen und die IP Adresse die sie dem Clent zugewiesen haben. ich habe hier bei dem Client die IP Adresse **192.168.100.53** verwendet jedoch können sie eine Belibige verwenden es muss einfach mit dem Privaten Netzwerk Kompatibel sein.

**Server**

![Alt text](Untitled4.png)

- Jetz kommt einer der wichtigsten schritte. Die IP Adressenverteilung dabei muss beachtet werden das diese Konfiguration bei dem Server wie auch bei dem Client vorgenommen werden muss.

- Zuerst nehem wir uns den Server vor. Hier geben wir beim Privaten Netzwerkt also in den Einstellungen **Adapter 1**. Dort schreibt man die IP Adresse von dem Server rein  ich habe hier die 192.168.100.5 gewählt + noch in den DNS einstellungen erneut die DNS IP Adresse und den Alternativen DNS dort habe ich den von Google genommen der ist **8.8.8.8**.

![Alt text](image-5.png)

- Dazu müssen wir beim Server noch beim **Adapter 0** nochmals nur die DNS einstellungen übernehemen jetz muss. Und somit währen wir in den in den Adaptereinstellungen von dem DNS Server fertig.

![Alt text](image-6.png)

- Nun muss nur noch der Google DNS als Primärer DNS Suffix eingestellt werden das wir nacher im testing auch Web seiten abrufen können. Dafür gehen wir im Server menü erneut auf **Tools** und wieder auf DNS. Hier gehen wir mit einem rechtsklick auf **WIN-KUKT...** und auf Properties.

![Alt text](image-7.png)

- Nun sind sie in den Eigenschafften hier gehen sie auf den reiter **Forwarders** und danach auf Edit... .

![Alt text](image-8.png)

- Und im blauen Feld tragen sie die IP Adresse ein also **8.8.8.8**. Sobalt der DNS akzeptiert wurde erscheint das grüne Symbol links von der IP Adresse.

**Client**

![Alt text](image-9.png)

-  Nun muss bei dem Client genau das selbe gemacht werden einfach das sie bei dem Privaten Adapter also erneut die Nummer 1. Aber die IP Adresse die sie dem Client zuweisen wollen ich habe hier die IP Adresse **192.168.100.53** gewählt. Und bei dem Unteren Fenster die selben DNS einstellungen wie bei dem Server.

![Alt text](image-10.png)

- Nun nur nochmals die selben DNS einstellungen beim Adapter 0. 

- Und schon ist die Konfiguration unseres DNS Servers abgeschlossen nun können wir zum Testing gehen.

# 3. Testing

![Alt text](image-11.png)

- Nun können wir mit dem Befehl **nslookup** herausfinden ob unser Server auch wircklich angenommen und ausgewählt ist. Wie man im bild sehen kann ist mein Server angenommen worden da beim Standartserver meiner (server.sota.com) ausgewählt ist.

![Alt text](image-12.png)

- Nun sehen wir das der **Ping** ebenfalls funktioniert. Jetzt folgt der Letze test. Wir wollen nun herausfinden ob unser Google DNS funktioniert und ob wir Websiten abrufen können.

![Alt text](image-13.png)

- Hier können wir sehen das die Website **www.Brack.ch** gefunden wurde und das unser Google DNS Funktioniert.


Danke fürs Lesen!

**END!**